package com.agilesolutions.springboot.service;

import com.agilesolutions.springboot.entity.AbstractEntity;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public interface IService {

    public AbstractEntity create(AbstractEntity entity);

    public AbstractEntity update(AbstractEntity entity);

    public boolean delete(long id) throws EntityNotFoundException;

    public AbstractEntity findBy(long id) throws EntityNotFoundException;

    public List readAll();

}
