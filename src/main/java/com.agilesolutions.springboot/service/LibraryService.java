package com.agilesolutions.springboot.service;

import com.agilesolutions.springboot.repository.LibraryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LibraryService extends AbstractService {

    @Autowired
    public LibraryService(LibraryRepository repository){
        super(repository);
    }

}
