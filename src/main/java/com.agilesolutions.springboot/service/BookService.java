package com.agilesolutions.springboot.service;


import com.agilesolutions.springboot.message.LogMessage;
import com.agilesolutions.springboot.repository.BookRepository;
import com.agilesolutions.springboot.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService extends AbstractService{


    private BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository repository){
        super(repository);
        this.bookRepository = repository;
    }

    public List<Book> getAllBooks(Long id){
        List<Book> books = new ArrayList<>();
        bookRepository.findByBookcaseId(id).forEach(books::add);
        logger.info(LogMessage.READ_ALL.getMessage());
        return books;
    }

}
