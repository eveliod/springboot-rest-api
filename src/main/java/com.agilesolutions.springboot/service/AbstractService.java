package com.agilesolutions.springboot.service;

import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.message.LogMessage;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.JpaRepository;

import org.slf4j.Logger;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;

public abstract class AbstractService implements IService{
    private final JpaRepository repository;
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    public AbstractService(JpaRepository repository){
        this.repository = repository;
    }

    @Override
    public AbstractEntity create(AbstractEntity entity){
        AbstractEntity saved = null;
        try{
            entity.setCreatedDate(LocalDateTime.now());
            entity.setUpdatedDate(LocalDateTime.now());
            saved = (AbstractEntity)repository.save(entity);
            logger.info(LogMessage.CREATE_OBJECT.getMessage());
        }
        catch (Exception error){
            logger.error(error.getMessage(), error);
        }

        return saved;
    }

    @Override
    public AbstractEntity update(AbstractEntity entity){
        AbstractEntity saved = null;
        try{
            entity.setUpdatedDate(LocalDateTime.now());
            saved = (AbstractEntity)repository.save(entity);
            logger.info(LogMessage.UPDATE_OBJECT.getMessage());
        }
        catch (Exception error){
            logger.error(error.getMessage(), error);
        }
        return saved;
    }

    @Override
    public boolean delete(long id) throws EntityNotFoundException {
        boolean result = true;
        try{
            if (repository.exists(id)){
                repository.delete(id);
                logger.info(LogMessage.DELETE_OBJECT.getMessage());
            }
            else {
                result = false;
                throw new EntityNotFoundException(Long.toString(id));
            }
        }
        catch (Exception error){
            logger.error(error.getMessage(), error);
        }

        return result;
    }

    @Override
    public AbstractEntity findBy(long id) throws EntityNotFoundException {
        AbstractEntity result = null;
        try{
            if (repository.exists(id)){
                result = (AbstractEntity) repository.findOne(id);
                logger.info(LogMessage.FIND_OBJECT.getMessage());
            }
            else {
                throw new EntityNotFoundException(Long.toString(id));
            }
        }
        catch (Exception error){
            logger.error(error.getMessage());
        }
        return result;
    }

    @Override
    public List<AbstractEntity> readAll() {
        List<AbstractEntity> all = null;
        try{
            logger.info(LogMessage.READ_ALL.getMessage());
            all = repository.findAll();
        }
        catch (Exception error){
            logger.error(error.getMessage(), error);
        }
        return all;
    }




}
