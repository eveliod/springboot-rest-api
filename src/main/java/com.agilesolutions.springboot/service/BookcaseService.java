package com.agilesolutions.springboot.service;

import com.agilesolutions.springboot.entity.Bookcase;
import com.agilesolutions.springboot.message.LogMessage;
import com.agilesolutions.springboot.repository.BookcasesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.ArrayList;

@Service
public class BookcaseService extends AbstractService {

    @Autowired
    private BookcasesRepository bookcasesRepository;
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public BookcaseService(BookcasesRepository repository){
        super(repository);
        this.bookcasesRepository = repository;
    }

    public List<Bookcase> getAllBookcases(Long id){
        List<Bookcase> bookcases = new ArrayList<>();
        bookcasesRepository.findByLibraryId(id).forEach(bookcases::add);
        logger.info(LogMessage.READ_ALL.getMessage());
        return bookcases;
    }



}
