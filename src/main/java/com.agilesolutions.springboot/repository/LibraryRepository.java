package com.agilesolutions.springboot.repository;

import com.agilesolutions.springboot.entity.Library;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(propagation = Propagation.MANDATORY)
public interface LibraryRepository extends JpaRepository<Library, Long> {
}