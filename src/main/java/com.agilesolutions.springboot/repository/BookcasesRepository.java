package com.agilesolutions.springboot.repository;

import com.agilesolutions.springboot.entity.Bookcase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface BookcasesRepository extends JpaRepository<Bookcase, Long> {
    public List<Bookcase> findByLibraryId(Long libraryId);
}
