package com.agilesolutions.springboot.exception;

public class ElementNotFoundException extends RuntimeException {
    private Long resourceId;

    public ElementNotFoundException(Long resourceId, String message) {
        super(message);
        this.resourceId = resourceId;
    }
}
