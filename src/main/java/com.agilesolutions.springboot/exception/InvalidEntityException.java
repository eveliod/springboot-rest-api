package com.agilesolutions.springboot.exception;

public class InvalidEntityException extends Exception{
    public InvalidEntityException() {
        super("Invalid Entity");
    }
}
