package com.agilesolutions.springboot.util;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExceptionResponse {
    private int statusError;
    private String nameError;
    private String errorMessage;
}
