package com.agilesolutions.springboot.controller;


import com.agilesolutions.springboot.exception.ElementNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.agilesolutions.springboot.service.LibraryService;
import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Library;
import java.util.List;

@RestController
@RequestMapping("/")
public class LibraryController  {

    @Autowired
    private LibraryService libraryService;

    @GetMapping("/library")
    public List<AbstractEntity> getAllLibraries(){
        return libraryService.readAll();
    }

    @GetMapping("/library/{id}")
    public AbstractEntity findBy(@PathVariable Long id){
        AbstractEntity library = libraryService.findBy(id);
        if (library!=null)
            return library;
        else
            throw new ElementNotFoundException(id,"Library not found.");
    }

    @PostMapping("/library")
    public void createLibrary(@RequestBody Library library){
        libraryService.create(library);
    }

    @PutMapping("/library")
    public void updateLibrary(@RequestBody Library library){
        libraryService.update(library);
    }

    @DeleteMapping("/library/{id}")
    public void deleteLibrary(@PathVariable String id){
        libraryService.delete(Long.parseLong(id));
    }

}
