package com.agilesolutions.springboot.controller;

import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Bookcase;
import com.agilesolutions.springboot.entity.Library;
import com.agilesolutions.springboot.exception.ElementNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.agilesolutions.springboot.service.BookcaseService;
import java.util.List;

@RestController
@RequestMapping("/")
public class BookcaseController {



    @Autowired
    private BookcaseService bookcaseService;

    @GetMapping("/bookcase")
    public List<AbstractEntity> getAllBookcase(){
        return bookcaseService.readAll();
    }

    @GetMapping("/library/{libraryId}/bookcase")
    public List<Bookcase> getAllBookcase(@PathVariable Long libraryId){
        List<Bookcase> bookcases = bookcaseService.getAllBookcases(libraryId);
        return bookcases;
        /*if (bookcases.size()!=0)
            return bookcases;
        else
            throw new ElementNotFoundException(libraryId,"No bookcases found in the current library.");*/
    }

    @GetMapping("/library/{libraryId}/bookcase/{id}")
    public AbstractEntity getBookcase(@PathVariable Long id){
        AbstractEntity bookcase =  bookcaseService.findBy(id);
        if (bookcase!=null)
            return bookcase;
        else
            throw new ElementNotFoundException(id, "Bookcase not found.");
    }

    @PostMapping("/library/{libraryId}/bookcase")
    public void createBookcase(@RequestBody Bookcase bookcase, @PathVariable Long libraryId){
        bookcase.setLibrary(new Library(libraryId,"",null));
        bookcaseService.create(bookcase);
    }

    @PutMapping("/library/{libraryId}/bookcase/{id}")
    public void updateBookcase(@RequestBody Bookcase bookcase, @PathVariable Long libraryId){
        bookcase.setLibrary(new Library(libraryId,"",null));
        bookcaseService.update(bookcase);
    }

    @DeleteMapping("/library/{libraryId}/bookcase/{id}")
    public void deleteBookcase(@PathVariable Long id){
        bookcaseService.delete(id);
    }


}
