package com.agilesolutions.springboot.controller;

import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Book;
import com.agilesolutions.springboot.entity.Bookcase;
import com.agilesolutions.springboot.entity.Library;
import com.agilesolutions.springboot.exception.ElementNotFoundException;
import com.agilesolutions.springboot.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@RestController
@RequestMapping("/")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/book")
    public List<AbstractEntity> getAllBooks(){
        return bookService.readAll();
    }

    @GetMapping("/library/{libraryId}/bookcase/{bookcaseId}/book")
    public List<Book> getAllBooksById(@PathVariable Long bookcaseId){
        return bookService.getAllBooks(bookcaseId);
    }

    @GetMapping("/library/{libraryId}/bookcase/{bookcaseId}/book/{id}")
    public Book getBook(@PathVariable Long id){
        Book book = (Book) bookService.findBy(id);
        if (book!=null)
            return book;
        else
            throw new ElementNotFoundException(id, "Book not found.");
    }

    @PostMapping("/library/{libraryId}/bookcase/{bookcaseId}/book")
    public void addBook(@RequestBody Book book, @PathVariable Long libraryId, @PathVariable Long bookcaseId){
        book.setBookcase(new Bookcase(bookcaseId,"","",new Library(libraryId,"",null),null));
        bookService.create(book);
    }

    @PutMapping("/library/{libraryId}/bookcase/{bookcaseId}/book/{id}")
    public void updateBook(@RequestBody Book book, @PathVariable Long libraryId, @PathVariable Long bookcaseId, @PathVariable Long id){
        //book.setBookcase(new Bookcase(bookcaseId,"","",new Library(libraryId,"",null),null));
        book.setId(id);
        bookService.update(book);
    }

    @DeleteMapping("/library/{libraryId}/bookcase/{bookcaseId}/book/{id}")
    public void deleteBook(@PathVariable Long id){
        bookService.delete(id);
    }
}
