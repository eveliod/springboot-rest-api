package com.agilesolutions.springboot.controller.handler;

import com.agilesolutions.springboot.exception.ElementNotFoundException;
import com.agilesolutions.springboot.exception.InvalidEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.bind.annotation.*;
import com.agilesolutions.springboot.util.ExceptionResponse;
import org.springframework.http.ResponseEntity;

import javax.persistence.EntityNotFoundException;


@RestControllerAdvice(annotations = RestController.class)
public class GlobalControllerAdvice {
    private static final Logger logger = LoggerFactory.getLogger(GlobalControllerAdvice.class);

    @ExceptionHandler({ElementNotFoundException.class, EntityNotFoundException.class, ResourceNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected ResponseEntity<ExceptionResponse> handleresourceNotFoundException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return generateResponse(ex, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({IllegalArgumentException.class, HttpMessageNotReadableException.class, InvalidEntityException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<ExceptionResponse> handleBadRequestException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return generateResponse(ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMediaTypeException.class)
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    protected ResponseEntity<ExceptionResponse> handleUnsupportedMediaTypeException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return generateResponse(ex, HttpStatus.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected ResponseEntity<ExceptionResponse> handleInternalServerException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return generateResponse(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ExceptionResponse> generateResponse(Exception ex, HttpStatus httpStatus){
        ExceptionResponse response = new ExceptionResponse();
        response.setStatusError(httpStatus.value());
        response.setNameError(httpStatus.name());
        response.setErrorMessage(ex.getMessage());
        return new ResponseEntity<ExceptionResponse>(response, httpStatus);
    }


}
