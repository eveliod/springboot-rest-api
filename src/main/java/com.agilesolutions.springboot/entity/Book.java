package com.agilesolutions.springboot.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;


@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "books")
@EqualsAndHashCode(callSuper = true)
public class Book extends AbstractEntity {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "author", nullable = false)
    private String author;

    @Column(name = "editorial", nullable = false)
    private String editorial;

    @Column(name = "published", nullable = false)
    private LocalDate published;

    @Column(name = "added", nullable = false)
    private LocalDate added;

    @Column(name = "quantity", nullable = false)
    private int quantity;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Bookcase.class)
    private Bookcase bookcase;


}
