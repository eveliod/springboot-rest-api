package com.agilesolutions.springboot.entity;


import lombok.Data;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
public abstract class AbstractEntity {

    @JsonProperty("created_date")
    @Column(name = "created_date", nullable = false)
    private LocalDateTime createdDate;

    @JsonProperty("updated_date")
    @Column(name = "updated_date")
    private LocalDateTime updatedDate;

}
