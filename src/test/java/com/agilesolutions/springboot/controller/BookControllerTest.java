package com.agilesolutions.springboot.controller;

import com.agilesolutions.springboot.Application;
import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Book;
import com.agilesolutions.springboot.entity.Bookcase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import org.junit.Before;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class BookControllerTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private BookController bookController;


    @Before
    public void configuration() throws Exception{
        this.mvc = webAppContextSetup(webApplicationContext).build();
    }


    @Test
    public void getAllBooks() throws Exception{
        Book book = new Book();
        book.setId(Long.parseLong("1"));
        book.setName("B1");

        List<AbstractEntity> allBooks = singletonList(book);

        given(bookController.getAllBooks()).willReturn(allBooks);

        mvc.perform(get("/book")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(book.getName())));
    }

   @Test
    public void getAllBooksById() throws Exception{
       Book book = new Book();
       book.setId(Long.parseLong("1"));
       book.setName("B1");

       List<Book> allBooks = singletonList(book);

       given(bookController.getAllBooksById(Long.parseLong("1"))).willReturn(allBooks);

       mvc.perform(get("/library/1/bookcase/1/book")
               .contentType(APPLICATION_JSON))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", hasSize(1)))
               .andExpect(jsonPath("$[0].name", is(book.getName())));
    }

    @Test
    public void getBook() throws Exception{

        Book book = new Book();
        book.setId(Long.parseLong("1"));
        book.setName("B1");


        given(bookController.getBook(Long.parseLong("1"))).willReturn(book);

        mvc.perform(get("/library/1/bookcase/1/book/1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(book.getName())));
    }

    @Test
    public void addBook() throws Exception{
        Book book = new Book();
        book.setId(Long.parseLong("1"));
        book.setName("B1");


        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(book);

        mvc.perform(post("/library/1/bookcase/1/book")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
    }

    @Test
    public void updateBook() throws Exception{
        Book book = new Book();
        book.setId(Long.parseLong("1"));
        book.setName("B1");


        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(book);

        mvc.perform(put("/library/1/bookcase/1/book/1")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteBook() throws Exception{
        Book book = new Book();
        book.setId(Long.parseLong("1"));
        book.setName("B1");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(book);

        mvc.perform(delete("/library/1/bookcase/1/book/1")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
    }
}
