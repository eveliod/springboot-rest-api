package com.agilesolutions.springboot.controller;

import com.agilesolutions.springboot.Application;
import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Library;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import org.junit.Before;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import static org.springframework.http.converter.json.Jackson2ObjectMapperBuilder.json;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class LibraryControllerTest {

    private MockMvc mvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private LibraryController libraryController;
    

    @Before
    public void configuration() throws Exception{
        this.mvc = webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void getAllLibraries() throws Exception {
        Library library = new Library();
        library.setId(Long.parseLong("1"));
        library.setName("Libreria Uno");

        List<AbstractEntity> allLibraries = singletonList(library);

        given(libraryController.getAllLibraries()).willReturn(allLibraries);

        mvc.perform(get("/library")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(library.getName())));
    }
    


    @Test
    public void findBy() throws Exception {
        Library library = new Library();
        library.setId(Long.parseLong("1"));
        library.setName("Libreria Uno");


        given(libraryController.findBy(Long.parseLong("1"))).willReturn(library);

        mvc.perform(get("/library/1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(library.getName())));
    }
    
    @Test
    public void createLibrary() throws Exception{
        Library library = new Library();
        library.setId(Long.parseLong("1"));
        library.setName("Libreria Uno");
        
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(library);
        
        mvc.perform(post("/library")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
    }

    @Test
    public void updateLibrary() throws Exception{
        Library library = new Library();
        library.setId(Long.parseLong("1"));
        library.setName("Libreria Uno");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(library);

        mvc.perform(put("/library")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteLibrary() throws Exception{
        Library library = new Library();
        library.setId(Long.parseLong("1"));
        library.setName("Libreria Uno");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(library);

        mvc.perform(delete("/library/1")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
    }



}