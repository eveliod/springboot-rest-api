package com.agilesolutions.springboot.controller;

import com.agilesolutions.springboot.Application;
import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Bookcase;
import com.agilesolutions.springboot.entity.Library;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import java.util.List;
import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import org.junit.Before;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class BookcaseControllerTest {
    
    private MockMvc mvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private BookcaseController bookcaseController;
    

    @Before
    public void configuration() throws Exception{
        this.mvc = webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void getAllBookcase() throws Exception{
        Bookcase bookcase = new Bookcase();
        Long id = Long.valueOf(1);
        bookcase.setId(id);
        bookcase.setName("B1");

        List<AbstractEntity> allBookcase = singletonList(bookcase);

        given(bookcaseController.getAllBookcase()).willReturn(allBookcase);

        mvc.perform(get("/bookcase")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(bookcase.getName())));

    }

    @Test
    public void getAllBookcaseByLibraryId() throws Exception{
       Bookcase bookcase = new Bookcase();
       bookcase.setId(Long.parseLong("1"));
       bookcase.setName("B1");
       
        List<Bookcase> allBookcase = singletonList(bookcase);

        given(bookcaseController.getAllBookcase(Long.parseLong("1"))).willReturn(allBookcase);

        mvc.perform(get("/library/1/bookcase")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(bookcase.getName())));
       
    }

    @Test
    public void getBookcase() throws Exception{
        Bookcase bookcase = new Bookcase();
        bookcase.setId(Long.parseLong("1"));
        bookcase.setName("B1");

        given(bookcaseController.getBookcase(Long.parseLong("1"))).willReturn(bookcase);

        mvc.perform(get("/library/1/bookcase/1")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(bookcase.getName())));
    }

    @Test
    public void createBookcase() throws Exception{
        Bookcase bookcase = new Bookcase();
        bookcase.setId(Long.parseLong("1"));
        bookcase.setName("B1");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(bookcase);

        mvc.perform(post("/library/1/bookcase")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
        
    }

    @Test
    public void updateBookcase() throws Exception{
        Bookcase bookcase = new Bookcase();
        bookcase.setId(Long.parseLong("1"));
        bookcase.setName("B1");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(bookcase);

        mvc.perform(put("/library/1/bookcase/1")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
    }

    public void deleteBookcase() throws Exception{
        Bookcase bookcase = new Bookcase();
        bookcase.setId(Long.parseLong("1"));
        bookcase.setName("B1");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = mapper.writeValueAsString(bookcase);

        mvc.perform(delete("/library/1/bookcase/1")
                .contentType(APPLICATION_JSON)
                .content(jsonInString))
                .andExpect(status().isOk());
    }
}
