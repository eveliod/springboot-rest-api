package com.agilesolutions.springboot.service;

import com.agilesolutions.springboot.entity.Book;
import com.agilesolutions.springboot.Application;
import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Bookcase;
import com.agilesolutions.springboot.entity.Library;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class BookServiceTest {

    @MockBean
    private BookService bookService;


    @Test
    public void readAll() {
        List<AbstractEntity> allBook = new ArrayList<>(Arrays.asList(
                new Book(Long.valueOf("1"),"Spring1","b","b", LocalDate.now(),LocalDate.now(),2, new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null)),
                new Book(Long.valueOf("2"),"Spring2","b","b", LocalDate.now(),LocalDate.now(),2, new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null))
        ));
        Mockito.when(bookService.readAll()).thenReturn(allBook);
        assertEquals(bookService.readAll(), allBook);
    }

    @Test
    public void findBy() {
        Book book = new Book(Long.valueOf("2"),"Spring2","b","b", LocalDate.now(),LocalDate.now(),2, new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null));
        Mockito.when(bookService.findBy(Long.valueOf("1"))).thenReturn(book);
        assertEquals(bookService.findBy(Long.valueOf("1")), book);
    }

    @Test
    public void create() {
        Book book = new Book(Long.valueOf("2"),"Spring2","b","b", LocalDate.now(),LocalDate.now(),2, new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null));
        Mockito.when(bookService.create(book)).thenReturn(book);
        assertEquals(bookService.create(book), book);
    }

    @Test
    public void update() {
        Book book = new Book(Long.valueOf("2"),"Spring2","b","b", LocalDate.now(),LocalDate.now(),2, new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null));
        Mockito.when(bookService.update(book)).thenReturn(book);
        assertEquals(bookService.update(book), book);
    }

    @Test
    public void delete() {
        Mockito.when(bookService.delete(Long.valueOf("1"))).thenReturn(true);
        assertEquals(bookService.delete(Long.valueOf("1")), true);
    }

    @Test
    public void getAllBookcases() {
        List<Book> allBook = new ArrayList<>(Arrays.asList(
                new Book(Long.valueOf("1"),"Spring1","b","b", LocalDate.now(),LocalDate.now(),2, new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null)),
                new Book(Long.valueOf("2"),"Spring2","b","b", LocalDate.now(),LocalDate.now(),2, new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null))
        ));
        Mockito.when(bookService.getAllBooks(Long.valueOf("1"))).thenReturn(allBook);
        assertEquals(bookService.getAllBooks(Long.valueOf("1")), allBook);
    }
}