package com.agilesolutions.springboot.service;

import com.agilesolutions.springboot.Application;
import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Bookcase;
import com.agilesolutions.springboot.entity.Library;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class BookcaseServiceTest {


    @MockBean
    private BookcaseService bookcaseService;


    @Test
    public void readAll() {
        List<AbstractEntity> allBookcase = new ArrayList<>(Arrays.asList(
                new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null),
                new Bookcase(Long.valueOf("2"),"Spring2","Spring",new Library(), null))
        );
        Mockito.when(bookcaseService.readAll()).thenReturn(allBookcase);
        assertEquals(bookcaseService.readAll(), allBookcase);
    }

    @Test
    public void findBy() {
        Bookcase bookcase = new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null);
        Mockito.when(bookcaseService.findBy(Long.valueOf("1"))).thenReturn(bookcase);
        assertEquals(bookcaseService.findBy(Long.valueOf("1")), bookcase);
    }

    @Test
    public void create() {
        Bookcase bookcase = new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null);
        Mockito.when(bookcaseService.create(bookcase)).thenReturn(bookcase);
        assertEquals(bookcaseService.create(bookcase), bookcase);
    }

    @Test
    public void update() {
        Bookcase bookcase = new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null);
        Mockito.when(bookcaseService.update(bookcase)).thenReturn(bookcase);
        assertEquals(bookcaseService.update(bookcase), bookcase);
    }

    @Test
    public void delete() {
        Mockito.when(bookcaseService.delete(Long.valueOf("1"))).thenReturn(true);
        assertEquals(bookcaseService.delete(Long.valueOf("1")), true);
    }

    @Test
    public void getAllBookcases() {
        List<Bookcase> allBookcase = new ArrayList<>(Arrays.asList(
                new Bookcase(Long.valueOf("1"),"Spring1","Spring",new Library(), null),
                new Bookcase(Long.valueOf("2"),"Spring2","Spring",new Library(), null))
        );
        Mockito.when(bookcaseService.getAllBookcases(Long.valueOf("1"))).thenReturn(allBookcase);
        assertEquals(bookcaseService.getAllBookcases(Long.valueOf("1")), allBookcase);
    }


}