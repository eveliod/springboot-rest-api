package com.agilesolutions.springboot.service;

import com.agilesolutions.springboot.Application;
import com.agilesolutions.springboot.entity.AbstractEntity;
import com.agilesolutions.springboot.entity.Library;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class LibraryServiceTest {


    @MockBean
    private LibraryService libraryService;


    @Test
    public void readAll() {
        List<AbstractEntity> allLibraries = new ArrayList<>(Arrays.asList(
                new Library(Long.valueOf("1"),"Spring",null),
                new Library(Long.valueOf("2"),"Spring",null))
        );
       Mockito.when(libraryService.readAll()).thenReturn(allLibraries);
       assertEquals(libraryService.readAll(), allLibraries);
    }

    @Test
    public void findBy() {
        Library library = new Library(Long.valueOf("1"),"Spring",null);
        Mockito.when(libraryService.findBy(Long.valueOf("1"))).thenReturn(library);
        assertEquals(libraryService.findBy(Long.valueOf("1")), library);
    }

    @Test
    public void create() {
        Library library = new Library(Long.valueOf("1"),"Spring",null);
        Mockito.when(libraryService.create(library)).thenReturn(library);
        assertEquals(libraryService.create(library), library);
    }

    @Test
    public void update() {
        Library library = new Library(Long.valueOf("1"),"Spring",null);
        Mockito.when(libraryService.update(library)).thenReturn(library);
        assertEquals(libraryService.update(library), library);
    }

    @Test
    public void delete() {
        Mockito.when(libraryService.delete(Long.valueOf("1"))).thenReturn(true);
        assertEquals(libraryService.delete(Long.valueOf("1")), true);
    }
}